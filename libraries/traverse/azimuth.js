class Azimuth {
  constructor(data) {
    this.known = data.azimuth_known;
    this.angles = data.angles;
    if (data.last_azimuth) this.check_azimuth = data.last_azimuth;
    this.app();
  }

  app() {
    this.getAzimuths();
  }

  isOver180(ang) {
    if (ang > 180) return true;
    return false;
  }

  formatOver180(ang) {
    if (this.isOver180(ang)) return ang - 180;
    return ang + 180;
  }

  isOver360(ang) {
    if (ang > 360) return true;
    return false;
  }

  formatOver360(ang) {
    if (this.isOver360(ang)) return ang - 360;
    return ang;
  }

  setAzimuth(prev_az, ang) {
    if (!isNaN(prev_az) && !isNaN(ang))
      return this.formatOver360(this.formatOver180(prev_az) + ang);
  }

  getAzimuths() {
    this.azimuts = [];
    let known = this.known ?? 0;
    for (const i of this.angles) {
      known = this.setAzimuth(known, i);
      this.azimuts.push(known);
    }
  }

  addKnownToTop() {
    this.azimuts.unshift(this.known);
  }

  deleteLast() {
    this.azimuts.pop();
  }
}

module.exports = Azimuth;
