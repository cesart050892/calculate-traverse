const Convertions = require("./convertions");
const Azimuth = require("./azimuth");
const Projections = require("./projections");
const Utils = require("./utils");

var fs = require("fs");

const con = new Convertions();
const ut = new Utils();
class Traverse {
  constructor(settings) {
    this.azimuth_known = con.getOneToDegree(settings.azimuth_known);
    this.angles = [];
    this.distances = [];
    for (const i of settings.all_data) {
      this.angles.push(i[0]);
      this.distances.push(i[1]);
    }
    this.bm_points = settings.benchmarks;
    this.angles = con.getToDegree(this.angles);
    if (settings.azimuth_check)
      this.last_azimuth = con.getOneToDegree(settings.azimuth_check);
    this.precision = settings.precision / 3600;
    this.app(settings.type);
    // this.runAzimuth();
  }

  app(type) {
    if (type === "close") {
      this.getTolerance();
      this.getSumAnglesInternals();
      this.getInternalAnglesError();
      this.checkTolerance();
      if (this.tolerance_status) {
        this.setAngleCompensation();
        this.runAzimuth();
        this.orderDistances();
        this.runProjections();
      }
    }
  }

  getTolerance() {
    this.tolerance = this.precision * Math.sqrt(this.angles.length);
  }

  getSumAnglesInternals() {
    let sum = 0;
    for (const i of this.angles) sum += i;
    this.sum_internal_angles = sum;
  }

  getInternalAnglesError() {
    let internal = 180 * (this.angles.length - 2);
    this.internal_angles_error = this.sum_internal_angles - internal;
  }

  checkTolerance() {
    if (this.tolerance > this.internal_angles_error)
      this.tolerance_status = true;
  }

  setAngleCompensation() {
    let compensation = this.internal_angles_error / this.angles.length;
    compensation *= -1;
    this.angles_compensated = [];
    for (let i = 0; i < this.angles.length; i++) {
      this.angles_compensated.push(this.angles[i] + compensation);
    }
  }

  runAzimuth() {
    let az = new Azimuth({
      azimuth_known: this.azimuth_known,
      angles: this.angles_compensated,
      // last_azimuth: this.last_azimuth,
    });
    az.addKnownToTop();
    az.deleteLast();
    this.azimuts = az.azimuts;
  }

  orderDistances() {
    let last_distance = this.distances[this.distances.length - 1];
    this.distances.pop();
    this.distances.unshift(last_distance);
  }

  runProjections() {
    let proj = new Projections({
      azimuts: this.azimuts,
      distances: this.distances,
      benchmark: this.bm_points,
    });
    this.deltas = proj.deltas;
    this.perimeter = proj.perimeter;
    this.linear_error = proj.linear_error;
    this.adjusted_projections = proj.adjusted_projections;
    this.getCoords();
    this.checkCoords(this.bm_points[0]);
  }

  getCoords() {
    this.coords = [];
    let prev_north = this.bm_points[0][0];
    let prev_east = this.bm_points[0][1];
    for (let i = 0; i < this.adjusted_projections.length; i++) {
      prev_north += this.adjusted_projections[i][0];
      prev_east += this.adjusted_projections[i][1];
      this.coords.push([
        ut.getRoundCeil(prev_north, 3),
        ut.getRoundCeil(prev_east, 3),
      ]);
    }
  }

  checkCoords(data) {
    let last_coord = this.coords[this.coords.length - 1];
    this.std_north = last_coord[0] - data[0];
    this.std_east = last_coord[1] - data[1];
    this.desviations = {
      "std north": Math.abs(ut.getRoundCeil(this.std_north, 3)),
      "std east": Math.abs(ut.getRoundCeil(this.std_east, 3)),
    };
  }

  info() {
    let data = {
      benckmarks: this.bm_points,
      coords: this.coords,
      std: this.desviations,
    };
    console.log(data);
  }

  exportCoordsToAutoCAD(dir) {
    let output= ""
    for (let i = 0; i < this.coords.length; i++) {
      output += `${this.coords[i][1]},${this.coords[i][0]}\n`
    }
    return output;
  }

  exportFile(filename) {
    let output = this.exportCoordsToAutoCAD();
    var fileContent = output;
    var filepath = `${filename}.txt`;

    fs.writeFile(filepath, fileContent, (err) => {
      if (err) throw err;

      console.log("The file was succesfully saved!");
    });
  }
}

module.exports = Traverse;
