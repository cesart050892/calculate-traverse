const Utils = require('./utils')
const ut = new Utils()
class Convertions {

  getOneToDegree(array) {
    return array[0] + array[1] / 60 + array[2] / 3600;
  }

  getOnetToSexagesimal(decimal) {
    let residual_g = decimal % 1;
    let grade = decimal - residual_g;
    let residual_m = (residual_g * 60) % 1;
    let minutes = residual_g * 60 - residual_m;
    let seconds = ut.getRoundCeil(residual_m * 60);
    return [grade, minutes, seconds];
  }

  getToDegree(angs) {
    let degrees = [];
    for (const i of angs) degrees.push(this.getOneToDegree(i));
    return degrees;
  }
}

module.exports = Convertions;
