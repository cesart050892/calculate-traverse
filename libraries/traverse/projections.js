const Utils = require("./utils");
const ut = new Utils();
class Projections {
  constructor(data) {
    this.azimuts = data.azimuts;
    this.distances = data.distances;
    this.benchmark_points = data.benchmark;
    this.app();
  }

  app() {
    this.getDeltas();
    this.getSumDeltas();
    this.getPerimeter();
    this.getlinearError();
    this.getDeltaFactors();
    this.getCompensations();
    this.getProjection();
  }

  getDegreesToRadians(degrees) {
    return degrees * (Math.PI / 180);
  }

  getDeltaNorth(distances, azimut) {
    return distances * Math.cos(this.getDegreesToRadians(azimut));
  }

  getDeltaEast(distances, azimut) {
    return distances * Math.sin(this.getDegreesToRadians(azimut));
  }

  getDeltas() {
    this.deltas = [];
    if (this.azimuts.length === this.distances.length) {
      for (let i = 0; i < this.azimuts.length; i++) {
        let north = this.getDeltaNorth(this.distances[i], this.azimuts[i]);
        let east = this.getDeltaEast(this.distances[i], this.azimuts[i]);
        this.deltas.push([ut.getRoundCeil(north, 3), ut.getRoundCeil(east, 3)]);
      }
    }
  }

  getSumDeltas() {
    this.sum_delta_north = 0;
    this.sum_delta_east = 0;
    for (let i = 0; i < this.deltas.length; i++) {
      this.sum_delta_north += this.deltas[i][0];
      this.sum_delta_east += this.deltas[i][1];
    }
  }

  getPerimeter() {
    let sum = 0;
    for (const i of this.distances) sum += i;
    this.perimeter = ut.getRoundCeil(sum, 3);
  }

  getlinearError() {
    let sum =
      Math.pow(this.sum_delta_north, 2) + Math.pow(this.sum_delta_north, 2);
    this.linear_error = ut.getRoundCeil(Math.sqrt(sum), 4);
  }

  getDiffBenchmarks() {
    let diff_north = ut.getRoundCeil(
      this.benchmark_points[1][0] - this.benchmark_points[0][0],
      3
    );
    let diff_east = ut.getRoundCeil(
      this.benchmark_points[1][1] - this.benchmark_points[0][1],
      3
    );
    return [diff_north, diff_east];
  }

  getDeltaFactors() {
    if (this.benchmark_points[1]) {
      let diff = this.getDiffBenchmarks();
      this.factor_north = (this.sum_delta_north - diff[0]) / this.perimeter;
      this.factor_east = (this.sum_delta_east - diff[1]) / this.perimeter;
    } else {
      this.factor_north = this.sum_delta_north / this.perimeter;
      this.factor_east = this.sum_delta_east / this.perimeter;
    }
  }

  getCompensations() {
    this.distances_compensations = [];
    for (let i = 0; i < this.distances.length; i++) {
      this.distances_compensations.push([
        ut.getRoundCeil(this.distances[i] * this.factor_north, 3),
        ut.getRoundCeil(this.distances[i] * this.factor_east, 3),
      ]);
    }
  }

  getProjection() {
    this.adjusted_projections = [];
    for (let i = 0; i < this.deltas.length; i++) {
      this.adjusted_projections.push([
        ut.getRoundCeil(
          this.deltas[i][0] - this.distances_compensations[i][0],
          3
        ),
        ut.getRoundCeil(
          this.deltas[i][1] - this.distances_compensations[i][1],
          3
        ),
      ]);
    }
  }
}

module.exports = Projections;
