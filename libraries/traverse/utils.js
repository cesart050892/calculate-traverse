class Utils {
  getRoundCeil(number, precision) {
    precision = precision ?? 0;
    return Number(number.toFixed(precision));
  }

  getSquared(val) {
    return val * val;
  }
}

module.exports = Utils;
