const Traverse = require("../libraries/traverse");
const Convertions = require("../libraries/traverse/convertions");

let dirs = [
  "../data/suggested.json",
  "../data/civil_engineering_tutoriales",
  "../data/ingenieria_paso_a_paso",
  "../data/libro_topografia_plana.json"
]

const data = require(dirs[3]);

const con = new Convertions();

const tr = new Traverse({
  azimuth_known: data.azimuth_known,
  all_data: data.data,
  azimuth_check: data.azimuth_check,
  benchmarks: data.benchmark_coords,
  precision: data.precision,
  type: data.type,
});

// for (let i = 0; i < tr.azimuts.length; i++) {
//   console.log(con.getOnetToSexagesimal(tr.azimuts[i]));
// }

/**
 * TODO:
 * Check Azimuts
 */

// let coords = data.coords

//  function exportCoordsToAutoCAD() {
//   for (let i = 0; i < coords.length; i++) {
//     console.log(`${coords[i][1]},${coords[i][0]}`);
//   }
// }

// exportCoordsToAutoCAD()
// console.log("\n")
tr.exportFile('coords')
